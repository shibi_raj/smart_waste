from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class MyUser(AbstractUser):
    is_customer = models.BooleanField(default=True)
    credits = models.FloatField(default=0)
    address = models.TextField(null=True, blank=True)
    phoneno = models.CharField(null=True, blank=True, max_length=100)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    place_id = models.CharField(max_length=64, blank=True, null=True)
    rating = models.IntegerField(default=1)


class Orders(models.Model):
    """
    Model to store order details
    """
    WASTE_CATEGORY = (
        ('O', 'Organic Waste'),
        ('P', 'Paper waste'),
        ('L', 'Plastic waste'),
        ('S', 'Scrap waste'),
        ('G', 'Glass waste'),
    )
    WASTE_SIZE = (
        ('S', 'Small'),
        ('M', 'Medium'),
        ('L', 'Large'),
    )
    REQUEST_STATUS = (
        ('P', 'Pending'),
        ('A', 'Accepted'),
        ('R', 'Rejected')
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='user_orders',
        on_delete=models.CASCADE
    )
    waste_category = models.CharField(max_length=1, choices=WASTE_CATEGORY)
    waste_size = models.CharField(max_length=1, choices=WASTE_SIZE)
    comments = models.TextField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    place_id = models.CharField(max_length=64, blank=True, null=True)
    request_status = models.CharField(max_length=1, choices=REQUEST_STATUS, default='P')

    provider = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='user_provider',
        on_delete=models.CASCADE, null=True, blank=True
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)




class Test(models.Model):
    """
    Model to store order details
    """
    WASTE_CATEGORY = (
        ('A', 'Accept'),
        ('P', 'Pending'),
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='user_orders_test',
        on_delete=models.CASCADE
    )
    waste_category = models.CharField(max_length=1, choices=WASTE_CATEGORY,default='P')
    comments = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)



