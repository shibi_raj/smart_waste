import requests
from app.models import *
from app.forms import *
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView, DetailView, View)
from django.contrib.auth.decorators import login_required, permission_required

from django.shortcuts import render
from django.views.generic import View
from django.http import JsonResponse
from django.shortcuts import redirect

# class CustomerListView(View):
#     def get(self,request):
#         return render(request,'home.html', )


# class CustomerListView(StudioUserRequiredMixin, ListView):
# @method_decorator(login_required, name='dispatch')
# class CustomerDashboardView(ListView):
#     # permissions = {
#     #     "any": ("aboutus.add_aboutus", "aboutus.change_aboutus", "aboutus.delete_aboutus")
#     #     }
#     model = Customer
#     template_name = 'home.html'
#     paginate_by = 10
#     ordering = '-updated_at'


class CustomerDashboardView(View):
    def get(self,request):
        return render(request,'dashboard.html' )


class OrderReviewView(View):
    def get(self, request, pk):
        providers = MyUser.objects.filter(is_customer=False).all()
        return render(request,'orders.html', {'providers' : providers, 'pk':pk})

    def post(self, request, pk):
        provider = request.POST['provider']
        Orders.objects.filter(pk=pk).update(provider_id=provider)
        return redirect("/order-list/")


class CustomerOrderView(CreateView):
    # form_class = CustomerForm
    # model = Test
    # template_name = 'test.html'
    # success_url = '/'
    #
    # def form_valid(self, form):
    #     user = MyUser.objects.first()
    #     form.instance.user = user
    #     # form.save()
    #     return super(CustomerOrderView, self).form_valid(form)

    #
    def post(self,request):
        latitude = request.POST['latitude'] if request.POST['latitude'] else None
        longitude = request.POST['longitude'] if request.POST['longitude'] else None
        waste_category = request.POST['waste_category']
        waste_size = request.POST['waste_size']
        comments = request.POST['comments']
        place_id = request.POST['place_id']

        order = Orders.objects.create(
            user=request.user,
            waste_category=waste_category,
            waste_size=waste_size,
            latitude=latitude,
            longitude=longitude,
            place_id=place_id,
            comments=comments,
        )

        return redirect("/provider-list/{}/".format(order.pk))
        return reverse('provider_list', args=(order.pk,))
        # return render(request,'dashboard.html' )


class UpdateOrderStatus(View):
    def post(self,request):
        request_id = request.POST['request_id']
        status = request.POST['status']
        Test.objects.filter(id=request_id).update(waste_category=status)

        return render(request,'test.html' )


class CustomerOrderListView(View):

    def get(self, request):
        # request_id = request.POST['request_id']
        # status = request.POST['status']
        # Test.objects.filter(id=request_id).update(waste_category=status)
        orders = Orders.objects.filter(user=request.user).all()

        return render(request, 'user_orders.html' , {'orders': orders})


class ProviderListView(View):

    def get(self, request, request_id):
        # request_id = request.POST['request_id']
        # status = request.POST['status']
        providers = MyUser.objects.filter(is_customer=False)
        order = Orders.objects.get(id=request_id)
        provider_list = []
        origin = "origins=%s,%s" % (order.latitude, order.longitude)
        DISTANCE_API = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC_0GFWlKVFi3JY4IluWGBF5FoEmTFyMYw"
        for provider in providers:
            provider.distance = '10 km'
            provider.duration = '20 mins'
            if provider.latitude and provider.longitude:
                destination = "destinations=%s,%s" % (provider.latitude, provider.longitude)
                dis_api = DISTANCE_API + '&' + origin + '&' + destination
                r = requests.get(dis_api)
                res = r.json()
                print (res)
                if 'rows' in res and 'elements' in res['rows'][0] and 'distance' in res['rows'][0]['elements'][0]:
                    provider.distance = res['rows'][0]['elements'][0]['distance']['text']
                    provider.duration = res['rows'][0]['elements'][0]['duration']['text']

            provider_list.append(provider)
        return render(request, 'providers.html', {"providers": provider_list, 'pk': request_id})

    def post(self, request, request_id):
        provider = request.POST['provider']
        Orders.objects.filter(pk=request_id).update(provider_id=provider)
        return redirect("/")


class ProviderDashboardView(View):
    def get(self,request):
        orders = Orders.objects.filter(request_status = 'P')
        order_list = []
        origin = "origins=%s,%s" % (request.user.latitude, request.user.longitude)
        DISTANCE_API = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyC_0GFWlKVFi3JY4IluWGBF5FoEmTFyMYw"
        for order in orders:
            order.distance = '10 km'
            order.duration = '20 mins'
            if order.latitude and order.longitude:
                destination = "destinations=%s,%s" % (order.latitude, order.longitude)
                dis_api = DISTANCE_API + '&' + origin + '&' + destination
                r = requests.get(dis_api)
                res = r.json()
                if 'rows' in res and 'elements' in res['rows'][0] and 'distance' in res['rows'][0]['elements'][0]:
                    order.distance = res['rows'][0]['elements'][0]['distance']['text']
                    order.duration = res['rows'][0]['elements'][0]['duration']['text']

            order_list.append(order)
        return render(request, 'provider_dashboard.html' , {"orders": order_list})


class ProviderHistoryView(View):

    def get(self, request):
        # request_id = request.POST['request_id']
        # status = request.POST['status']
        orders = Orders.objects.filter(request_status__in=['R', 'A'])

        return render(request, 'provider_history.html' , {"orders": orders})


class ProviderActionView(View):

    def post(self, request):
        print (request.POST)
        order_id = request.POST['request_id']
        status = request.POST['status']
        order = Orders.objects.get(id=order_id)
        order.request_status = status
        order.save()
        return JsonResponse({'order_status': order.request_status})


class ProviderMapView(View):

    def get(self, request):
        # request_id = request.POST['request_id']
        # status = request.POST['status']
        orders = Orders.objects.filter(request_status__in=['R', 'A'])
        return render(request, 'map.html' , {"orders": orders})
