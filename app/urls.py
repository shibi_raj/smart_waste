from django.conf.urls import url
from app.views import *

# # apis
# urlpatterns = [
#     url(r'^api/customers$', CustomerListAPIView.as_view({'get': 'list'})),
#     url(r'^api/customer/(?P<pk>[0-9]+)$', CustomerAPIView.as_view()),
#     url(r'^api/login$', CustomerLoginAPIView.as_view()),
#     url(r'^api/get-customer-name$', CustomerDetailsAPIView.as_view()),
# ]

# django web views
urlpatterns = [
    url(r'^$', CustomerDashboardView.as_view(), name="customer_dashboard"),
    url(r'order$', CustomerOrderView.as_view(), name="customer_order"),
    url(r'order-review/(?P<pk>\d+)/$', OrderReviewView.as_view(), name="order_review"),
    url(r'order-list/$', CustomerOrderListView.as_view(), name="list_orders"),
    url(r'update-status$', UpdateOrderStatus.as_view(), name="update_status"),
    url(r'provider-list/(?P<request_id>\d+)/$', ProviderListView.as_view(), name="provider_list"),

    # Provider views

    url(r'provider/$', ProviderDashboardView.as_view(), name="provider_dashboard"),
    url(r'provider/history/$', ProviderHistoryView.as_view(), name="provider_histor y"),
    url(r'provider/action/$', ProviderActionView.as_view(), name="provider_action"),
    url(r'provider/map/$', ProviderMapView.as_view(), name="provider_map"),

    # url(r'^login$', LoginView.as_view(), name="login"),
    # url(r'^create/$',  CustomerCreateView.as_view(), name="create"),
    # url(r'^detail/(?P<pk>\d+)/$',  CustomerDetailView.as_view(), name="details"),
    # url(r'^update/(?P<pk>\d+)/$',  CustomerUpdateView.as_view(), name="update"),
    # url(r'^delete/(?P<pk>\d+)/$',  CustomerDeleteView.as_view(), name="delete"),
]