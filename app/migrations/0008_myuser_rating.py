# Generated by Django 2.1.4 on 2019-07-14 02:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20190714_0754'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='rating',
            field=models.IntegerField(default=1),
        ),
    ]
