# Generated by Django 2.0.6 on 2019-07-14 04:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_myuser_phoneno'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='place_id',
            field=models.CharField(blank=True, max_length=64, null=True),
        ),
    ]
