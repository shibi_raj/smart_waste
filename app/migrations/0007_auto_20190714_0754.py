# Generated by Django 2.1.4 on 2019-07-14 02:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_myuser_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='latitude',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='myuser',
            name='longitude',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
