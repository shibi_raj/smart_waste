# Generated by Django 2.1.4 on 2019-07-14 02:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_orders_provider'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='address',
            field=models.TextField(blank=True, null=True),
        ),
    ]
