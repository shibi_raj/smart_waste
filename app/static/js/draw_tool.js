$(document).ready(function() {
var $flowchart = $('#example');
    var $container = $flowchart.parent();

    var cx = $flowchart.width() / 2;
    var cy = $flowchart.height() / 2;

    if (workflow && workflow !='None'){
            var data = workflow;
    }
    else{
        var data = {'operators': {'0': {'properties': {'title': '<i class="fa fa-car c-1 fa-3x" id="cab-list" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 260, 'top': 40}, '1': {'properties': {'title': '<i class="fa fa-plane c-2 fa-3x" id="flight-list" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 0, 'top': 100}, '2': {'properties': {'title': '<i class="fa fa-hotel c-3 fa-3x" id="stay-list" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 140, 'top': 60}, '3': {'properties': {'title': '<i class="fa fa-check-square-o fa-3x c-4" id="approval" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 400, 'top': 80}, '4': {'properties': {'title': '<i class="fa fa-ticket c-5 fa-3x" id="ticket" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 480, 'top': 180}, '5': {'properties': {'title': '<i class="fa fa-file-o c-6 fa-3x" id="upload" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 580, 'top': 280}, '6': {'properties': {'title': '<i class="fa fa-money c-7 fa-3x" id="finance" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {'output_0': {'label': ''}}}, 'left': 680, 'top': 360}, '7': {'properties': {'title': '<i class="fa fa-hand-paper-o c-8 fa-3x" id="close" style="padding-top:5px; padding-left:5px;"></i>', 'inputs': {'input_0': {'label': ''}}, 'outputs': {}}, 'left': 820, 'top': 440}}, 'links': {'0': {'fromOperator': 1, 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': 2, 'toConnector': 'input_0', 'toSubConnector': 0, 'id': '0'}, '1': {'fromOperator': '0', 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': 3, 'toConnector': 'input_0', 'toSubConnector': 0, 'id': 1}, '2': {'fromOperator': '2', 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': '0', 'toConnector': 'input_0', 'toSubConnector': 0, 'id': '2'}, '3': {'fromOperator': 3, 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': '4', 'toConnector': 'input_0', 'toSubConnector': 0, 'id': 3}, '4': {'fromOperator': '4', 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': 5, 'toConnector': 'input_0', 'toSubConnector': 0, 'id': '4'}, '5': {'fromOperator': 5, 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': 6, 'toConnector': 'input_0', 'toSubConnector': 0, 'id': '5'}, '6': {'fromOperator': 6, 'fromConnector': 'output_0', 'fromSubConnector': 0, 'toOperator': 7, 'toConnector': 'input_0', 'toSubConnector': 0, 'id': '6'}}, 'operatorTypes': {}};
    }
     $.each( data.operators, function( k, v ) {
        if(v.properties){
            var title = v.properties.title;
            var boxes = ['car','approval', 'plane', 'hotel', 'visa', 'wifi'];
            $.each(boxes, function( l, m ) {
                if(title.indexOf(m) > -1){
                    var t = $('<i/>').html(v.properties.title).contents().addClass('granted').prop('outerHTML');
                    console.log(t);
                    v.properties.title = t;
                    if (m == 'approval'){
                        return false;
                    }
                }
            });
        }
     });


    var links = [];
    // Apply the plugin on a standard, empty div...
    $flowchart.flowchart({
        data: data,
        linkWidth: 3,
        defaultLinkColor: '#999999',
        multipleLinksOnOutput: true,
            onLinkCreate: function (linkId, linkData) {
              linkData['id'] = linkId;
              links = [];
              links.push(linkData['toOperator']);
              return true;
          },
          onLinkDelete: function (linkId, linkData) {
              return true;
          },
          onOperatorDelete: function (operatorId) {
              return true;
          },
          onAfterChange: function (changeType){

            if (changeType == 'link_create'){
                if (wifi){
                    update_price();
                    wifi = false;
                }

              call_server();
            }
          }
    });
//    if (approved){
         $('.granted').parent().parent().css('box-shadow','0px 1px 6px 1px rgba(0, 128, 0, 0.87)');
//    }
    $('.delete_selected_button').click(function() {
      $flowchart.flowchart('deleteSelected');
    });
    var wifi = false;

    var $draggableOperators = $('.action-type');

    function getOperatorData($element) {
      var nbInputs = parseInt($element.data('nb-inputs'));
      var nbOutputs = parseInt($element.data('nb-outputs'));
      var type = $element.data("type");
      var connect_order = $element.data("order");
      var title;
      if (type == 'flight'){
        title = '<i class="fa fa-plane fa-3x c-2" id="flight-list" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if (type == 'hotel'){
        title = '<i class="fa fa-hotel fa-3x c-3" id="stay-list" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if (type == 'cars'){
        title = '<i class="fa fa-car fa-3x c-1" id="cab-list" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='approve'){
        title = '<i class="fa fa-check-square-o fa-3x c-4" id="approval" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='ticket'){
        title = '<i class="fa fa-ticket fa-3x c-5" id="ticket" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='file'){
        title = '<i class="fa fa-file-o fa-3x c-6" id="upload" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='money'){
        title = '<i class="fa fa-money fa-3x c-7" id="finance" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='close'){
         title = '<i class="fa fa-hand-paper-o fa-3x c-8" id="close" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='wifi'){
      wifi = true;
         title = '<i class="fa fa-wifi fa-3x" id="close" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='cutlery'){
         title = '<i class="fa fa-cutlery fa-3x" id="cutlery" style="padding-top:5px; padding-left:5px;"></i>';
      }
      else if(type=='visa'){
         title = '<i class="fa fa-file-text fa-3x c-6" id="close" style="padding-top:5px; padding-left:5px;"></i>';
      }
      var data = {
        properties: {
          title: title,
          inputs: {},
          outputs: {}
        }
      };
      var i = 0;
      for (i = 0; i < nbInputs; i++) {
        data.properties.inputs['input_' + i] = {
          label: ''
        };
      }
      for (i = 0; i < nbOutputs; i++) {
        data.properties.outputs['output_' + i] = {
          label: ''
        };
      }

      return data;
    }

    var operatorId = 0;

    $draggableOperators.draggable({
        cursor: "move",
        opacity: 0.7,

        helper: 'clone',
        appendTo: 'body',
        zIndex: 1000,

        helper: function(e) {
          var $this = $(this);
          var data = getOperatorData($this);
          return $flowchart.flowchart('getOperatorElement', data);
        },
        stop: function(e, ui) {
            var $this = $(this);
            var elOffset = ui.offset;
            var containerOffset = $container.offset();
            if (elOffset.left > containerOffset.left &&
                elOffset.top > containerOffset.top &&
                elOffset.left < containerOffset.left + $container.width() &&
                elOffset.top < containerOffset.top + $container.height()) {

                var flowchartOffset = $flowchart.offset();

                var relativeLeft = elOffset.left - flowchartOffset.left;
                var relativeTop = elOffset.top - flowchartOffset.top;

                var positionRatio = $flowchart.flowchart('getPositionRatio');
                relativeLeft /= positionRatio;
                relativeTop /= positionRatio;

                var data = getOperatorData($this);
                data.left = relativeLeft;
                data.top = relativeTop;

                $flowchart.flowchart('addOperator', data);
            }
        }
    });

  $('[data-toggle="tooltip"]').tooltip();

    function get_link( links, varCurrent)
        {


            var operator_data = $flowchart.flowchart('getData')['links'];

            var current ="";


            $.each( operator_data, function( key, value ) {
                if (value['toOperator'] == varCurrent) {
                    links.push(value['fromOperator']);
                    current = value['fromOperator'];

                    get_link(links, current);
                    return false
                }

            });

        }

    $(document).on('click', '#stay-list', function(){
      $('#hotelModel').modal('show');
        $.ajax({
                    url: '/get-hotel/',
                    type: 'GET',
                    success: function (data) {
                       console.log(data);
                       $('#hotelModel .table-list').html(data.response);
                    },
                });
    });

    $(document).on('click', '#cab-list', function(){
      $('#cabModel').modal('show');
        $.ajax({
                    url: '/get-car/',
                    type: 'GET',
                    success: function (data) {
                       console.log(data);
                       $('#cabModel .table-list').html(data.response);
                    },
                });
    });



   $(document).on('click', '#flight-list', function(){
      $('#flightModel').modal('show');
      $.ajax({
            url: '/get-flight/',
            type: 'GET',
            success: function (data) {
               console.log(data);
               $('#flightModel .table-list').html(data.response);
            },
        });
    });

    $(document).on('click', '#ticket', function(){
      $('#confirmModel').modal('show');
      $.ajax({
            url: '/get-summary/',
            type: 'GET',
            success: function (data) {
               console.log(data);
               $('#confirmModel .table-list').html(data.response);
            },
        });
    });

    $(document).on('click', '#approval', function(){
      $('#approvalModel').modal('show');
    });


    function call_server(){
            console.log('Inside the function -:- call_server')

            var operator_data = $flowchart.flowchart('getData');
            get_link(links, links[0]);
            // Ajax call for data processing
            var data = JSON.stringify(operator_data);
            var new_links = JSON.stringify(links);
             $.ajax({
                    url: '/model-data/',
                    type: 'POST',
                    data: {"data": data, "links": new_links},
                    success: function (data) {
                       console.log(data);
                    },
                });
      }

$(document).on('change', '#toggle-check', function(){
      $('.alert-warning').toggleClass("showit");
      $('#ripple').toggleClass("circle-ripple");
    })
function update_price(){

    var box = '<li class="row card-box wifi"><span class="itemName item">Wifi &nbsp;</span><span class="price m-l-20">€ 10</span></li>';
    $('.price_list').append(box);
    $('.total-price').remove();

    var total = '<li class="row card-box total-price" style="background-color:#7ace7a; color:white;"><span class="itemName item" style=" color:white;">Total:</span><span class="price m-l-20" style="color:white;">€ 807</span></li>';
    $('.price_list').append(total);
}
});

