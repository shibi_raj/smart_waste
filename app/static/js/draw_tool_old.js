$(document).ready(function() {
    var data = {
      operators: {
        }
    };

    var loader = '<div class="theme-loader"><div class="ball-scale"><div class="contain"><div class="ring">'+
              '<div class="frame"></div></div><div class="ring"><div class="frame"></div></div><div class="ring">'+
              '<div class="frame"></div></div><div class="ring"><div class="frame"></div>'+
              '</div><div class="ring"><div class="frame"></div></div><div class="ring"><div class="frame"></div>'+
              '</div><div class="ring"><div class="frame"></div></div>'+
              '<div class="ring"><div class="frame"></div></div>'+
              '<div class="ring"><div class="frame"></div></div>'+
              '<div class="ring"><div class="frame"></div></div></div></div></div>';
    /*var $flowchart = $('#example').flowchart({
      data: data,
      // allows to add links by clicking on lines
      canUserEditLinks: true,

      // enables drag and drop
      canUserMoveOperators: true,

      // distance between the output line and the link
      distanceFromArrow: 3,

      // default operator class
      defaultOperatorClass: 'flowchart-default-operator',

      // default color
      defaultLinkColor: '#999',

      // default link color
      defaultSelectedLinkColor: 'orange',

      // width of the links
      linkWidth: 3,

      // <a href="https://www.jqueryscript.net/tags.php?/grid/">grid</a> of the operators when moved
      grid: 20,

      // allows multiple links on the same input line
      multipleLinksOnOutput: false,

      // allows multiple links on the same output line
      multipleLinksOnInput: false,

      // Allows to vertical decal the links (in case you override the CSS and links are not aligned with their
      // connectors anymore).
      linkVerticalDecal: 0,

      // callbacks
      onOperatorSelect: function (operatorId) {
          return true;
      },
      onOperatorUnselect: function () {
          return true;
      },
      onOperatorMouseOver: function (operatorId) {
          return true;
      },
      onOperatorMouseOut: function (operatorId) {
          return true;
      },
      onLinkSelect: function (linkId) {
          return true;
      },
      onLinkUnselect: function () {
          return true;
      },
      onOperatorCreate: function (operatorId, operatorData, fullElement) {
          return true;
      },
      onLinkCreate: function (linkId, linkData) {
          return true;
      },
      onOperatorDelete: function (operatorId) {
          return true;
      },
      onLinkDelete: function (linkId, forced) {
          return true;
      },
      onOperatorMoved: function (operatorId, position) {

      },
      onAfterChange: function (changeType) {

      }
    });*/
    var links = [];
    var $flowchart = $('#example').flowchart({
       data: data,
       linkWidth: 3,
       defaultLinkColor: '#999999',
       multipleLinksOnOutput: true,
       multipleLinksOnOutput: true,
       onLinkCreate: function (linkId, linkData) {

          //var connect_order = ui.draggable.attr("data-order");

          linkData['id'] = linkId;
          links = [];
          links.push(linkData['toOperator']);
          return true;

      },
      onLinkDelete: function (linkId, linkData) {
          return true;
      },
      onOperatorDelete: function (operatorId) {
          return true;
      },
      onAfterChange: function (changeType){

        if (changeType == 'link_create'){
          call_server();
        }
      }
     });

    var operatorI = 0;

    $flowchart.siblings('.delete_selected_button').click(function() {
      $flowchart.flowchart('deleteSelected');
    });

    function get_link( links, varCurrent)
    {


        var operator_data = $flowchart.flowchart('getData')['links'];

        var current ="";


        $.each( operator_data, function( key, value ) {
            if (value['toOperator'] == varCurrent) {
                links.push(value['fromOperator']);
                current = value['fromOperator'];

                get_link(links, current);
                return false
            }

        });

    }

  var onDragEnter = function(event) {
      event.preventDefault();
      $("#example").addClass("dragover");
  };

  onDragOver = function(event) {
      event.preventDefault();
      if(!$("#example").hasClass("dragover"))
          $("#example").addClass("dragover");
  };

  onDragLeave = function(event) {
      event.preventDefault();
      $("#example").removeClass("dragover");
  };

  $(".action-type").draggable();

  $("#example").droppable({
       drop: function(event, ui) {
          event.preventDefault();
          var type = ui.draggable.attr("data-type");
          var connect_order = ui.draggable.attr("data-order");

          var cls = ui.draggable.attr('class').split(" ");
          if (cls.indexOf('action-type')  > -1 ){
              ui.draggable.css("left",0).css("top",0);
              var title;
              if (type == 'file'){
                title = '<i class="feather icon-file" id="file-handler" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'table-list'){
                title = '<i class="fa fa-table" id="table-list" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'algo'){
                title = '<i class="fa fa-cogs" id="algo" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'info'){
                title = '<i class="fa fa-info" id="info" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'outlier'){
                title = '<i class="fa fa-braille" id="outlier" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'bar'){
                title = '<i class="fa fa-bar-chart" id="bar" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'feature-sel'){
                title = '<i class="fa fa-check-square-o" id="feature-sel" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'scatter-plot'){
                title = '<i class="fa fa-scatter-chart" id="scatter-plot" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'balance-data'){
                title = '<i class="fa fa-balance-scale" id="balance-data" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'line-chart'){
                title = '<i class="fa fa-line-chart" id="line-chart" style="padding-top:5px; padding-left:5px;"></i>';
              }
              else if (type == 'scale-data'){
                title = '<i class="fa fa-equals" id="scale-data" style="padding-top:5px; padding-left:5px;"></i>';
              }

              var dropPositionX = event.pageX - $("#example").offset().left;
              var dropPositionY = event.pageY - $("#example").offset().top;
              $("#example").removeClass("dragover");
              var input_label = 'input_'+operatorI;
              var output_label = 'output_'+operatorI;
              var data = {
                top: dropPositionY,
                left: dropPositionX,
                properties: {
                  title: title,
                  inputs: {
                  },
                }
              };
              data.properties.inputs[input_label] = {
                      label: '',
                    }
              if ((type == 'file') || (type == 'outlier') || (type == 'algo') || (type == 'feature-sel') || (type == 'balance-data') || (type == 'scale-data')){
                data.properties['outputs'] = {};
                data.properties['outputs'][output_label] = {
                              label: '',
                            }
                }
              var operatorId = type+'_'+connect_order + '_' + operatorI;
              operatorI++;
              $flowchart.flowchart('createOperator', operatorId, data);
              $('.del-opt').remove();
              $('.flowchart-operator').append('<span class="del-opt">X</span>');
          }
       }
  });

    $("#example")
      .on("dragenter", onDragEnter)
      .on("dragover", onDragOver)
      .on("dragleave", onDragLeave)

    $(document).on('click', '#file-handler', function(){
      $('#uploadModel').modal('show');
    });

    $(document).on('click', '#table-list', function(){
      $('#tableListModel').modal('show');
    });

    $(document).on('click', '#algo', function(){
      $('#algoModel').modal('show');
    });

    $(document).on('click', '#bar', function(){
      $('#BarModel').modal('show');
    });

    $(document).on('click', '#line-chart', function(){
      $('#linechartModel').modal('show');
    });

    $(document).on('click', '#scatter-plot', function(){
      $('#scatterchartModel').modal('show');
    });

    $(document).on('click', '#feature-sel', function(){
      $('#feature-selModel').modal('show');
    });

    $(document).on('click', '#balance-data', function(){
      $('#balancingModel').modal('show');
    });

    $(document).on('click', '#scale-data', function(){
      $('#datascalingModel').modal('show');
    });



    $(document).on('click', '#info', function(){
      $('#infoModel').modal('show');
    });

    //If no schema is selected disable drawing board and show add schema buttson
    //if (!schemaId ){
     // $(".main-body").addClass("disabledbutton");
      //$(".pcoded-inner-content").append('<div class="add-schema add-schema-btn">Create a schema <i class="fa fa-plus-square"></i> </div>')
   // }

    $(document).on('click', '.add-schema-btn', function(){
      $('#AddSchema').modal('show');
    });

    $(document).on('click', '.del-opt', function(){
       $flowchart.flowchart('deleteSelected');
    });

//    $(document).on('dblclick',".flowchart-operator", function() {
//      alert( "Handler for .dblclick() called." );
//    });


    // Ajax call for uploading dataset
    $("form#data").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
         $.ajax({
                url: '/uploads/add/',
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#uploadModel').modal('hide');
                    swal({
                        title: "Successfully uploaded the file!",
                        type: "success",
                    },
                    function() {

                    });
                },
                cache: false,
                contentType: false,
                processData: false,
                error: function (data) {
                  console.log(data);
                },
            });
    });

    // Ajax call for creating schema
    $("form#add-schema").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
         $.ajax({
                url: '/workflow/add/',
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('#AddSchema').modal('hide');
                    $(".main-body").removeClass("disabledbutton");
                    swal({
                        title: "Schema created successfully!",
                        type: "success",
                    },
                    function() {

                    });
                },
                cache: false,
                contentType: false,
                processData: false
            });
    });

  function call_server(){

        console.log('Inside -:- call_server')

        var operator_data = $flowchart.flowchart('getData');
        get_link(links, links[0]);
        console.log('Inside -:- call_server -> After -:- get_link')
//        $('body').append(loader);
        // Ajax call for data processing
        var data = JSON.stringify(operator_data, null, 2);
        var new_links = JSON.stringify(links);
         $.ajax({
                url: '/model-data/',
                type: 'POST',
                data: {"data": data, "links": new_links},
                success: function (data) {
//                $('.theme-loader').remove();
                    $('.theme-loader').fadeIn('slow');
                  if (data['link'] == 'table-list' ){
                    $('#tableListModel').modal('show');
                    $('.table-list').html(data['response']);
                  }


                },
            });
  }

  $('[data-toggle="tooltip"]').tooltip();

});



